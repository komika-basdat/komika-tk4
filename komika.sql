--
-- PostgreSQL database dump
--

-- Dumped from database version 10.10 (Ubuntu 10.10-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.10 (Ubuntu 10.10-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: DATABASE postgres; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON DATABASE postgres IS 'default administrative connection database';


--
-- Name: komika; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA komika;


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: cek_buku_baca(); Type: FUNCTION; Schema: komika; Owner: -
--

CREATE FUNCTION komika.cek_buku_baca() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE current_baca SMALLINT;
BEGIN
SELECT SUM(P.Jumlah) INTO current_baca
FROM PESANAN_KOMIK_BACA AS P
WHERE NEW.No_Anggota = P.No_Anggota AND P.Status = 'Baca';

IF current_baca IS NULL THEN
current_baca := 0;
END IF;

IF (TG_OP = 'INSERT') THEN
IF (current_baca + NEW.Jumlah_komik) > 3 THEN
RAISE EXCEPTION 'Jumlah komik melebihi 3';
END IF;

RETURN NEW;

END IF;
END;
$$;


--
-- Name: cek_buku_pinjam(); Type: FUNCTION; Schema: komika; Owner: -
--

CREATE FUNCTION komika.cek_buku_pinjam() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE current_pinjam SMALLINT;
BEGIN
SELECT SUM(P.Jumlah) INTO current_pinjam
FROM PESANAN_KOMIK_PINJAM AS P
WHERE NEW.No_Anggota = P.No_Anggota AND P.Status = 'Pinjam';

IF current_pinjam IS NULL THEN
current_pinjam := 0;
END IF;

IF (TG_OP = 'INSERT') THEN
IF (current_pinjam + NEW.Jumlah_komik) > 5 THEN
RAISE EXCEPTION 'Jumlah komik melebihi 5';
END IF;
RETURN NEW;
END IF;
END;
$$;


--
-- Name: cek_durasi(); Type: FUNCTION; Schema: komika; Owner: -
--

CREATE FUNCTION komika.cek_durasi() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
IF(TG_OP = 'INSERT') THEN
IF (NEW.Durasi_pinjam <> 3 AND NEW.Durasi_pinjam <> 5 AND NEW.Durasi_pinjam <> 7) THEN
RAISE EXCEPTION 'Durasi tidak valid';
END IF;
RETURN NEW;
END IF;
END;
$$;


--
-- Name: kembalikan_buku(character varying, character varying); Type: FUNCTION; Schema: komika; Owner: -
--

CREATE FUNCTION komika.kembalikan_buku(timestmp character varying, no_ang character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE current date := now();

BEGIN
UPDATE TRANSAKSI_PINJAM
SET Tanggal_dikembalikan = current, Durasi_pinjam = Tanggal_dikembalikan - Tanggal_pinjam, Denda = 5000 * (Tanggal_dikembalikan - Tanggal_kembali)
WHERE TRANSAKSI_PINJAM.Timestamp = timestmp
AND TRANSAKSI_PINJAM.No_Anggota = no_ang;

END;
$$;


--
-- Name: selesai_baca(character varying, character varying, character varying); Type: FUNCTION; Schema: komika; Owner: -
--

CREATE FUNCTION komika.selesai_baca(no_urut character varying, id_komik character varying, no_ang character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
UPDATE PESANAN_KOMIK_BACA
SET Status = 'Selesai Baca'
WHERE PESANAN_KOMIK_BACA.Nomor_urut = no_urut
AND PESANAN_KOMIK_BACA.id = id_komik
AND PESANAN_KOMIK_BACA.No_Anggota = no_ang;
END;
$$;


--
-- Name: selesai_pinjam(character varying, character varying, character varying); Type: FUNCTION; Schema: komika; Owner: -
--

CREATE FUNCTION komika.selesai_pinjam(no_urut character varying, id_komik character varying, no_ang character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
UPDATE PESANAN_KOMIK_PINJAM
SET Status = 'Sudah Dikembalikan'
WHERE PESANAN_KOMIK_PINJAM.Nomor_urut = no_urut
AND PESANAN_KOMIK_PINJAM.id = id_komik
AND PESANAN_KOMIK_PINJAM.No_Anggota = no_ang;
END;
$$;


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: anggota; Type: TABLE; Schema: komika; Owner: -
--

CREATE TABLE komika.anggota (
    no_anggota character varying(10) NOT NULL,
    id character varying(20) NOT NULL,
    nama character varying(255) NOT NULL,
    alamat text NOT NULL,
    tanggal_lahir date NOT NULL,
    no_hp character varying(15) NOT NULL,
    ang_pembaca character varying(255) NOT NULL,
    ang_peminjam character varying(255) NOT NULL
);


--
-- Name: bank_mitra; Type: TABLE; Schema: komika; Owner: -
--

CREATE TABLE komika.bank_mitra (
    kode character varying(5) NOT NULL,
    nama_bank character varying(255) NOT NULL
);


--
-- Name: komik; Type: TABLE; Schema: komika; Owner: -
--

CREATE TABLE komika.komik (
    id character varying(10) NOT NULL,
    seri character varying(255) NOT NULL,
    jumlah_available smallint NOT NULL
);


--
-- Name: menu; Type: TABLE; Schema: komika; Owner: -
--

CREATE TABLE komika.menu (
    id character varying(10) NOT NULL,
    harga integer NOT NULL
);


--
-- Name: pesanan_komik_baca; Type: TABLE; Schema: komika; Owner: -
--

CREATE TABLE komika.pesanan_komik_baca (
    nomor_urut integer NOT NULL,
    jumlah integer NOT NULL,
    status character varying(255) NOT NULL,
    id character varying(10) NOT NULL,
    timestamp_baca character varying(10) NOT NULL,
    no_anggota character varying(10) NOT NULL
);


--
-- Name: pesanan_komik_pinjam; Type: TABLE; Schema: komika; Owner: -
--

CREATE TABLE komika.pesanan_komik_pinjam (
    nomor_urut integer NOT NULL,
    jumlah integer NOT NULL,
    status character varying(255) NOT NULL,
    id character varying(10) NOT NULL,
    timestamp_pinjam character varying(10) NOT NULL,
    no_anggota character varying(10) NOT NULL
);


--
-- Name: pesanan_menu_trbaca; Type: TABLE; Schema: komika; Owner: -
--

CREATE TABLE komika.pesanan_menu_trbaca (
    nomor_urut integer NOT NULL,
    jumlah integer NOT NULL,
    id character varying(10) NOT NULL,
    timestamp_baca character varying(10) NOT NULL,
    no_anggota character varying(10) NOT NULL
);


--
-- Name: pesanan_menu_trpinjam; Type: TABLE; Schema: komika; Owner: -
--

CREATE TABLE komika.pesanan_menu_trpinjam (
    nomor_urut integer NOT NULL,
    jumlah integer NOT NULL,
    id character varying(10) NOT NULL,
    timestamp_pinjam character varying(10) NOT NULL,
    no_anggota character varying(10) NOT NULL
);


--
-- Name: petugas; Type: TABLE; Schema: komika; Owner: -
--

CREATE TABLE komika.petugas (
    username character varying(255) NOT NULL,
    password character varying(255) NOT NULL
);


--
-- Name: produk; Type: TABLE; Schema: komika; Owner: -
--

CREATE TABLE komika.produk (
    id character varying(10) NOT NULL,
    nama character varying(255) NOT NULL,
    jumlah smallint NOT NULL,
    tipe_produk character varying(255) NOT NULL
);


--
-- Name: transaksi_baca; Type: TABLE; Schema: komika; Owner: -
--

CREATE TABLE komika.transaksi_baca (
    "timestamp" character varying(10) NOT NULL,
    jumlah_komik smallint NOT NULL,
    jumlah_menu smallint NOT NULL,
    total_biaya integer NOT NULL,
    no_anggota character varying(10) NOT NULL,
    kode character varying(5) NOT NULL
);


--
-- Name: transaksi_pinjam; Type: TABLE; Schema: komika; Owner: -
--

CREATE TABLE komika.transaksi_pinjam (
    "timestamp" character varying(10) NOT NULL,
    jumlah_komik smallint NOT NULL,
    jumlah_menu smallint NOT NULL,
    total_biaya integer NOT NULL,
    denda integer NOT NULL,
    durasi_pinjam integer NOT NULL,
    tanggal_pinjam date NOT NULL,
    tanggal_kembali date NOT NULL,
    tanggal_dikembalikan date NOT NULL,
    no_anggota character varying(10) NOT NULL,
    kode character varying(5) NOT NULL
);


--
-- Data for Name: anggota; Type: TABLE DATA; Schema: komika; Owner: -
--

COPY komika.anggota (no_anggota, id, nama, alamat, tanggal_lahir, no_hp, ang_pembaca, ang_peminjam) FROM stdin;
16353	qc5rB5vPFQ	Gwenyth Smith	BectiveRue, 299	1982-06-21	3-232-853-7260	tidak	peminjam
5039	mRRLl3OfQl	Louise Lewin	CentralDrive, 4561	1983-05-21	8-372-218-1514	pembaca	tidak
961	J9poZ0SRa1	Charlotte Groves	AyresWay, 6916	1992-03-12	4-216-708-0565	pembaca	peminjam
21854	H5fPTx2G9f	Benny Blackwall	Buttonwood Hill, 8938	1996-01-02	5-286-017-5376	tidak	peminjam
25695	eWifIbRHlL	Rufus Potter	CollingwoodTunnel, 992	1989-01-02	2-764-114-0414	tidak	tidak
8921	9rBVBYX0Pl	William Spencer	Bayberry Drive, 4838	1993-11-13	3-320-783-3637	tidak	tidak
4902	4Mw0OuZqGc	Ramon Taylor	BinghamRoute, 6407	1996-06-09	8-088-662-3506	pembaca	tidak
12645	p2KRNntAwq	Sebastian Camden	ArthurWalk, 979	1984-02-17	3-180-020-1827	tidak	tidak
11885	CijyMW7VTp	Maxwell York	UnwinRoad, 4343	1995-12-31	7-002-671-8146	tidak	peminjam
29057	D02sMSTaNV	Tyler Edwards	BermondseyRue, 3250	1990-06-21	3-063-827-0037	pembaca	tidak
16797	7NVzHrs1kX	Mason Jackson	CalvinWalk, 9146	1993-03-17	2-828-248-3222	pembaca	tidak
12347	eegiUeJ4f8	Harriet Matthews	CarltounPass, 9870	1994-03-25	5-255-520-3223	tidak	peminjam
9195	SPLfQWxVim	Sydney Miller	GautreyBoulevard, 2282	1984-12-24	3-031-777-2218	tidak	tidak
3018	YeVzgePlF0	Aurelia Davies	Spruce Rue, 1678	1984-03-18	3-011-712-5304	pembaca	peminjam
18779	8oi98ZNaou	Daron Edler	BelAvenue, 297	1986-06-27	4-520-124-6277	pembaca	peminjam
17323	mcpMIK8ZY3	Mayleen Redfern	WadhamDrive, 5148	1992-07-25	6-060-652-6468	tidak	peminjam
24119	Nsn3yzBgVD	Percy Maxwell	CharterhousePass, 3375	1983-12-13	3-174-848-5744	pembaca	tidak
12278	T3FJUmxbbM	Eryn Willis	CarolineTunnel, 1485	1987-04-23	5-016-316-4722	pembaca	peminjam
25831	VrZopL9VRg	Brooklyn Strong	AyresHill, 3134	1987-09-05	0-803-026-8518	pembaca	peminjam
6483	Ign32eKyYL	Ada Morris	Cleaver Pass, 9915	1993-10-06	4-388-222-3013	tidak	peminjam
10042	Tsx34aRTkA	Ron Fox	TiptreeLane, 8154	1986-04-19	8-867-153-5501	tidak	tidak
238	A9Lj5Ft9At	Bree Robinson	BlackwallAlley, 4673	1983-04-27	8-124-585-2476	pembaca	tidak
12723	ZpvwrPjkkN	Leslie Saunders	BiglandRoute, 8431	1992-07-06	1-767-645-5271	pembaca	peminjam
8198	dc9nVqtPVt	Daron Clark	West Tunnel, 7027	1987-02-01	6-605-670-2052	tidak	peminjam
11173	FmqZZMKOYx	Sienna Uttley	ChalcotCrossroad, 3478	1984-05-08	0-535-372-7245	pembaca	tidak
1497	753wlSFlkV	Bryon Wild	TillochAvenue, 3317	1983-12-08	3-766-602-4137	tidak	peminjam
21908	IvIc6jomD3	Juliette Lewis	King Pass, 678	1988-02-03	4-422-211-6268	tidak	peminjam
21510	jlshV2DGTg	Chadwick Camden	BalfeGrove, 6066	1988-03-11	5-221-848-2673	tidak	tidak
3136	rusdBgCDsf	Crystal Jobson	BennettBoulevard, 9813	1982-02-11	5-654-237-0352	tidak	tidak
28603	oQNfsIPsGI	Daniel Benfield	Rivervalley Tunnel, 306	1986-01-10	2-223-123-0145	pembaca	tidak
11496	NPTlHdCdPU	Shannon Sherry	ChatfieldPass, 2466	1994-01-08	1-185-380-2200	pembaca	tidak
15206	rggy5aEbbo	Gil Bishop	TiptreePass, 4976	1992-05-29	5-021-258-6026	tidak	peminjam
5886	H96GNaNWwE	Georgia Pearson	LittleburyRue, 6157	1994-05-11	0-553-332-2860	pembaca	peminjam
21036	N20wwXTttC	Hayden Chester	ClereWay, 3065	1987-12-14	4-534-454-6812	tidak	tidak
22506	28VC0zaLXY	Jennifer Hancock	Buttonwood Grove, 7653	1994-10-30	2-550-722-2253	pembaca	tidak
15304	YHHnQQCeCQ	Javier Plant	Jackson Drive, 8244	1987-07-04	7-531-284-6451	pembaca	tidak
130	1zMlSHdtRL	Elijah Mills	Hickory Lane Crossroad, 8386	1996-10-08	5-358-088-6744	pembaca	peminjam
25078	rKWLuNzohN	Harry Ashley	Sheffield Crossroad, 7419	1993-09-22	4-685-171-5336	pembaca	peminjam
23627	SXlUeomPQw	Alba Durrant	EndsleighHill, 8451	1988-12-10	1-330-651-2553	pembaca	peminjam
25545	ByExQZiAuy	Mackenzie Clark	WadhamBoulevard, 1696	1995-02-09	6-503-827-6330	tidak	tidak
24986	fNMOfCxtIH	Havana Parker	BerrimanAlley, 8707	1992-03-06	8-152-841-4361	pembaca	tidak
20637	HZcpJ5xSxf	Jazmin Carter	CaldwellPass, 8036	1988-06-16	3-765-881-7343	tidak	peminjam
27588	dtfrmMFCM6	Cara Lane	Howard Crossroad, 3796	1993-11-28	1-727-364-6814	tidak	tidak
12730	cn81NFjvZT	Carl Hilton	CliftonDrive, 8820	1994-10-05	2-870-171-8041	pembaca	tidak
22723	LtMZ2ffPso	Hailey Amstead	AddisonDrive, 8056	1982-07-08	0-344-557-8014	pembaca	peminjam
7720	ZXr0vdMUiv	Kaylee Squire	Rivervalley Avenue, 9457	1982-03-07	7-072-625-7043	tidak	peminjam
6412	WhHzQ7ALcw	Bob Trent	CanalStreet, 5444	1981-07-21	8-041-500-5151	tidak	tidak
4480	pU5gYHnWJO	Jacob Lee	CamRue, 7367	1987-05-09	3-501-824-6237	tidak	peminjam
9452	8XAnQBovcZ	Alexander Cavanagh	SheringhamDrive, 3458	1990-10-02	7-834-510-1733	pembaca	peminjam
9497	pDR2nGyCIZ	Anthony Bullock	King WilliamTunnel, 8051	1993-02-16	7-663-573-3277	tidak	peminjam
\.


--
-- Data for Name: bank_mitra; Type: TABLE DATA; Schema: komika; Owner: -
--

COPY komika.bank_mitra (kode, nama_bank) FROM stdin;
24094	Goldward Financial
27050	Excellence Financial Group
23403	Lifespark Bancshares
28514	Vertex Bank Corp
29664	Ace Corporation
31685	New Alliance Trust Corp
24007	Evolution Corporation
24174	Premium Financial Holdings
31835	Focus Banks Inc
29032	New Alliance Bank
31924	Paramount Financial Inc
31230	Obelisk Trust Corp
28239	Life Tree Bank Inc
25054	Champion Financial Services
\.


--
-- Data for Name: komik; Type: TABLE DATA; Schema: komika; Owner: -
--

COPY komika.komik (id, seri, jumlah_available) FROM stdin;
P0000	11737	57
P0001	20161	22
P0002	12262	43
P0003	20527	57
P0004	12981	118
P0005	12805	120
P0006	11132	82
P0007	21589	100
P0008	22491	23
P0009	22994	15
P0010	21859	36
P0011	10806	20
P0012	11049	31
P0013	20266	114
P0014	20405	63
P0015	20426	82
P0016	20912	122
P0017	12253	117
P0018	21214	30
P0019	11848	118
P0020	10453	128
P0021	22248	92
P0022	22824	33
P0023	20378	11
P0024	22751	46
P0025	22558	83
P0026	21603	81
P0027	11968	73
P0028	20727	74
P0029	21446	110
P0030	21294	48
P0031	21702	124
P0032	21938	50
P0033	10979	20
P0034	12493	70
P0035	22799	129
P0036	22343	111
P0037	12676	21
P0038	10155	62
P0039	20108	106
P0040	11283	106
P0041	11432	83
P0042	10745	23
P0043	12571	103
P0044	22271	105
P0045	10718	74
P0046	11565	92
P0047	12787	66
P0048	12875	107
P0049	21921	47
P0050	11036	128
P0051	12365	85
P0052	22218	31
P0053	11244	33
P0054	20455	39
P0055	12135	74
P0056	11686	122
\.


--
-- Data for Name: menu; Type: TABLE DATA; Schema: komika; Owner: -
--

COPY komika.menu (id, harga) FROM stdin;
P0057	131000
P0058	100000
P0059	55000
P0060	48000
P0061	136000
P0062	43000
P0063	108000
P0064	72000
P0065	115000
P0066	65000
P0067	123000
P0068	29000
P0069	76000
P0070	82000
P0071	119000
P0072	69000
P0073	115000
P0074	33000
P0075	47000
P0076	67000
P0077	124000
P0078	121000
P0079	31000
P0080	44000
P0081	109000
P0082	104000
P0083	98000
P0084	117000
P0085	36000
P0086	107000
P0087	128000
P0088	25000
P0089	111000
P0090	58000
P0091	110000
P0092	70000
P0093	51000
P0094	123000
P0095	72000
P0096	89000
P0097	98000
P0098	116000
P0099	128000
P0100	53000
P0101	119000
P0102	65000
P0103	106000
P0104	71000
P0105	131000
P0106	125000
P0107	126000
P0108	88000
P0109	43000
P0110	57000
P0111	76000
P0112	128000
P0113	28000
P0114	86000
\.


--
-- Data for Name: pesanan_komik_baca; Type: TABLE DATA; Schema: komika; Owner: -
--

COPY komika.pesanan_komik_baca (nomor_urut, jumlah, status, id, timestamp_baca, no_anggota) FROM stdin;
1	1	Baca	P0043	2019-09-11	238
2	1	Baca	P0053	2018-10-06	15304
3	2	Baca	P0028	2019-10-07	16797
4	2	Baca	P0013	2018-03-23	4902
5	1	Baca	P0054	2018-01-02	11173
6	3	Baca	P0023	2018-03-17	22506
7	2	Baca	P0024	2018-09-05	25078
8	3	Baca	P0020	2019-01-05	25831
9	1	Baca	P0056	2019-01-12	3018
10	2	Baca	P0042	2018-04-05	12723
11	2	Baca	P0008	2019-10-23	5886
12	3	Baca	P0008	2019-05-13	9452
13	2	Baca	P0031	2019-12-14	4902
14	1	Baca	P0018	2019-11-05	11173
15	3	Baca	P0036	2018-10-17	22506
16	3	Baca	P0054	2018-05-13	9452
17	2	Baca	P0010	2018-08-06	3018
18	3	Baca	P0030	2019-04-06	961
19	1	Baca	P0025	2019-01-23	130
20	3	Baca	P0040	2018-11-21	12723
21	3	Baca	P0002	2019-02-14	12278
22	3	Baca	P0053	2018-02-16	12723
\.


--
-- Data for Name: pesanan_komik_pinjam; Type: TABLE DATA; Schema: komika; Owner: -
--

COPY komika.pesanan_komik_pinjam (nomor_urut, jumlah, status, id, timestamp_pinjam, no_anggota) FROM stdin;
1	4	Selesai Pinjam	P0009	2019-08-24	21908
2	5	Selesai Pinjam	P0013	2018-12-17	16353
3	3	Selesai Pinjam	P0019	2018-08-23	17323
4	2	Selesai Pinjam	P0051	2019-09-03	25078
5	1	Selesai Pinjam	P0027	2018-04-02	17323
6	5	Selesai Pinjam	P0008	2019-11-29	5886
7	5	Selesai Pinjam	P0023	2019-02-09	20637
8	1	Selesai Pinjam	P0051	2018-09-04	1497
9	3	Selesai Pinjam	P0043	2018-07-11	961
10	3	Selesai Pinjam	P0025	2019-11-01	21854
11	3	Selesai Pinjam	P0041	2018-12-12	18779
12	2	Selesai Pinjam	P0015	2019-12-07	23627
13	3	Selesai Pinjam	P0010	2018-04-01	6483
14	2	Selesai Pinjam	P0047	2019-11-13	12723
15	2	Selesai Pinjam	P0005	2019-08-13	25831
16	3	Selesai Pinjam	P0029	2018-01-09	21908
17	4	Selesai Pinjam	P0007	2019-12-03	9452
18	3	Selesai Pinjam	P0032	2018-04-25	15206
19	3	Selesai Pinjam	P0037	2018-05-24	15206
20	3	Selesai Pinjam	P0043	2019-07-05	9497
21	5	Selesai Pinjam	P0024	2018-07-22	6483
\.


--
-- Data for Name: pesanan_menu_trbaca; Type: TABLE DATA; Schema: komika; Owner: -
--

COPY komika.pesanan_menu_trbaca (nomor_urut, jumlah, id, timestamp_baca, no_anggota) FROM stdin;
1	5	P0086	2019-09-11	238
2	1	P0095	2018-10-06	15304
3	3	P0081	2019-10-07	16797
4	5	P0074	2018-03-23	4902
5	3	P0092	2018-01-02	11173
6	4	P0087	2018-03-17	22506
7	5	P0071	2018-09-05	25078
8	4	P0111	2019-01-05	25831
9	1	P0090	2019-01-12	3018
10	5	P0108	2018-04-05	12723
11	5	P0068	2019-10-23	5886
12	5	P0089	2019-05-13	9452
13	2	P0087	2019-12-14	4902
14	1	P0063	2019-11-05	11173
15	4	P0105	2018-10-17	22506
16	2	P0063	2018-05-13	9452
17	5	P0094	2018-08-06	3018
18	2	P0089	2019-04-06	961
19	2	P0094	2019-01-23	130
20	3	P0080	2018-11-21	12723
21	2	P0095	2019-02-14	12278
22	3	P0062	2018-02-16	12723
\.


--
-- Data for Name: pesanan_menu_trpinjam; Type: TABLE DATA; Schema: komika; Owner: -
--

COPY komika.pesanan_menu_trpinjam (nomor_urut, jumlah, id, timestamp_pinjam, no_anggota) FROM stdin;
1	5	P0059	2019-08-24	21908
2	5	P0090	2018-12-17	16353
3	1	P0072	2018-08-23	17323
4	5	P0067	2019-09-03	25078
5	5	P0092	2018-04-02	17323
6	3	P0098	2019-11-29	5886
7	3	P0087	2019-02-09	20637
8	2	P0074	2018-09-04	1497
9	0	P0061	2018-07-11	961
10	6	P0076	2019-11-01	21854
11	3	P0061	2018-12-12	18779
12	6	P0090	2019-12-07	23627
13	7	P0087	2018-04-01	6483
14	6	P0061	2019-11-13	12723
15	1	P0063	2019-08-13	25831
16	0	P0113	2018-01-09	21908
17	0	P0062	2019-12-03	9452
18	4	P0089	2018-04-25	15206
19	7	P0090	2018-05-24	15206
20	0	P0066	2019-07-05	9497
21	2	P0086	2018-07-22	6483
\.


--
-- Data for Name: petugas; Type: TABLE DATA; Schema: komika; Owner: -
--

COPY komika.petugas (username, password) FROM stdin;
1180063426	ae077ff109327e609290cf45a1ae51af569cbe334116cbca2c65300cdffa4293c89f40f87455b02f7ebee0e5c460999188448d93a479f70191cdcfbed21883c2
\.


--
-- Data for Name: produk; Type: TABLE DATA; Schema: komika; Owner: -
--

COPY komika.produk (id, nama, jumlah, tipe_produk) FROM stdin;
P0000	Batman: The Brave and the Bold	62	KOMIK
P0001	The Centurions	27	KOMIK
P0002	Spider-Woman	51	KOMIK
P0003	Teen Titans	74	KOMIK
P0004	Teen Titans Go!	129	KOMIK
P0005	Visionaries: Knights of the Magical Light	122	KOMIK
P0006	Tom and Jerry	96	KOMIK
P0007	Batman Beyond	108	KOMIK
P0008	Soul Eater	40	KOMIK
P0009	A Certain Magical Index	33	KOMIK
P0010	Mushibugyo	36	KOMIK
P0011	The Avengers: United They Stand	22	KOMIK
P0012	Cyborg 009	36	KOMIK
P0013	Tom and Jerry Tales	116	KOMIK
P0014	Gargoyles	65	KOMIK
P0015	Teenage Mutant Ninja Turtles	90	KOMIK
P0016	Black Panther	129	KOMIK
P0017	Spider-Man Unlimited	123	KOMIK
P0018	Hataraku Maou-sama!	49	KOMIK
P0019	Fantastic Four	133	KOMIK
P0020	Sora no Otoshimono	130	KOMIK
P0021	The Fantastic Four	98	KOMIK
P0022	Teenage Mutant Ninja Turtles	40	KOMIK
P0023	Kaze no Stigma	28	KOMIK
P0024	Super Robot Wars	49	KOMIK
P0025	Iron Man: The Animated Series	101	KOMIK
P0026	Dragon Ball GT	95	KOMIK
P0027	Teenage Mutant Ninja Turtles	77	KOMIK
P0028	The Incredible Hulk	83	KOMIK
P0029	The Flash	115	KOMIK
P0030	Shazam!	48	KOMIK
P0031	Human Target	137	KOMIK
P0032	Firefly	51	KOMIK
P0033	Star Wars: The Clone Wars	22	KOMIK
P0034	Lois & Clark: The New Adventures of Superman	71	KOMIK
P0035	Adventures of Superman	140	KOMIK
P0036	Birds of Prey	125	KOMIK
P0037	Static Shock	27	KOMIK
P0038	Legion of Super Heroes	69	KOMIK
P0039	Blade: The Series	108	KOMIK
P0040	Baka to Test to Shoukanjuu	121	KOMIK
P0041	The Super Hero Squad Show	90	KOMIK
P0042	Stargate SG-1	42	KOMIK
P0043	Axe Cop	123	KOMIK
P0044	Power Rangers	116	KOMIK
P0045	Hi Hi Puffy AmiYumi	79	KOMIK
P0046	Superboy	96	KOMIK
P0047	The Smurfs	66	KOMIK
P0048	Bucky O'Hare and the Toad Wars	114	KOMIK
P0049	The New Batman Adventures	55	KOMIK
P0050	Flash Gordon	134	KOMIK
P0051	Krypto the Superdog	102	KOMIK
P0052	Denpa Onna to Seishun Otoko	51	KOMIK
P0053	Superman	36	KOMIK
P0054	Medaka Box	40	KOMIK
P0055	Angel	78	KOMIK
P0056	Reborn!	132	KOMIK
P0057	Galliano and halloumi salad	77	MENU
P0058	Hazelnut and raspberry cookies	119	MENU
P0059	Popcorn and fish cake	108	MENU
P0060	Peanut and rabbit salad	82	MENU
P0061	Mushroom and rambutan korma	36	MENU
P0062	Borscht and mint salad	88	MENU
P0063	Fish and pepper ciabatta	126	MENU
P0064	Peppercorn and game pie	54	MENU
P0065	Roast chicken	68	MENU
P0066	Chilli and banana bread	89	MENU
P0067	Coley and feta salad	122	MENU
P0068	Peppercorn and bacon penne	97	MENU
P0069	Tahini and tofu panini	45	MENU
P0070	Tofu and anise curry	80	MENU
P0071	Fish and stilton salad	47	MENU
P0072	Brie and cucumber bagel	136	MENU
P0073	Squash and coffee cake	131	MENU
P0074	Lamb and bean stew	49	MENU
P0075	Cheese and ricotta pizza	76	MENU
P0076	Beef and bean lasagne	131	MENU
P0077	Cocoa and pecan crepes	110	MENU
P0078	Tofu and ricotta parcels	117	MENU
P0079	Apple and orange bread	27	MENU
P0080	Orange extract and kumquat salad	58	MENU
P0081	Kohlrabi and borscht soup	133	MENU
P0082	Gruyere and crayfish toastie	136	MENU
P0083	Rosemary and buffalo salad	131	MENU
P0084	Ginger and steak casserole	20	MENU
P0085	Caraway and pineapple cake	73	MENU
P0086	Italian sausage and cod salad	36	MENU
P0087	Bulgur wheat and chicory salad	121	MENU
P0088	Falafel and lamb burgers	74	MENU
P0089	Oatmeal tart with garlic sauce	45	MENU
P0090	Wheat and squash salad	124	MENU
P0091	Costmary and socca salad	107	MENU
P0092	Coriander and prune buns	91	MENU
P0093	Sweetcorn and cheese parcels	139	MENU
P0094	Pepper and feta spaghetti	43	MENU
P0095	Peppercorn and chickpea salad	87	MENU
P0096	Paneer and lamb skewers	103	MENU
P0097	Avocado and caper spaghetti	116	MENU
P0098	Bacon and fish pie	99	MENU
P0099	Turkey and aubergine spaghetti	135	MENU
P0100	Truffle and parmesan soup	71	MENU
P0101	Blackcurrant cordial and lime peel salad	76	MENU
P0102	Apple and pesto pizza	112	MENU
P0103	Lettuce and chickpea toastie	97	MENU
P0104	Banana and bran cookies	132	MENU
P0105	Coriander and horseradish dumplings	62	MENU
P0106	Pear and sharon fruit salad	44	MENU
P0107	Pineapple and fennel soup	42	MENU
P0108	Sultana and coriander biscuits	106	MENU
P0109	Eel and nutmeg cake	60	MENU
P0110	Aubergine and bean madras	127	MENU
P0111	Tomato and pork tenderloin salad	137	MENU
P0112	Strawberry and treacle buns	24	MENU
P0113	Rosemary and lemon buns	39	MENU
P0114	Hazelnut and loquat jam	77	MENU
\.


--
-- Data for Name: transaksi_baca; Type: TABLE DATA; Schema: komika; Owner: -
--

COPY komika.transaksi_baca ("timestamp", jumlah_komik, jumlah_menu, total_biaya, no_anggota, kode) FROM stdin;
2019-09-11	1	5	535000	238	24094
2018-10-17	3	4	524000	22506	24094
2019-10-07	2	3	327000	16797	27050
2018-08-06	2	5	615000	3018	27050
2018-03-23	2	5	165000	4902	23403
2019-04-06	3	2	222000	961	23403
2018-01-02	1	3	210000	11173	28514
2019-01-23	1	2	246000	130	28514
2018-03-17	3	4	512000	22506	29664
2018-11-21	3	3	132000	12723	29664
2018-09-05	2	5	595000	25078	31685
2019-02-14	3	2	144000	12278	31685
2019-01-05	3	4	304000	25831	24007
2018-02-16	3	3	129000	12723	24007
2019-01-12	1	1	58000	3018	24174
2018-04-05	2	5	440000	12723	31835
2019-10-23	2	5	145000	5886	29032
2019-05-13	3	5	555000	9452	31924
2019-12-14	2	2	256000	4902	31230
2019-11-05	1	1	108000	11173	28239
2018-10-06	1	1	72000	15304	25054
2018-05-13	3	2	216000	9452	25054
\.


--
-- Data for Name: transaksi_pinjam; Type: TABLE DATA; Schema: komika; Owner: -
--

COPY komika.transaksi_pinjam ("timestamp", jumlah_komik, jumlah_menu, total_biaya, denda, durasi_pinjam, tanggal_pinjam, tanggal_kembali, tanggal_dikembalikan, no_anggota, kode) FROM stdin;
2019-08-24	4	5	275000	0	7	2019-08-24	2019-08-31	2019-08-28	21908	24094
2019-08-13	2	1	138000	30000	3	2019-08-13	2019-08-16	2019-08-22	25831	24094
2018-08-23	3	1	84000	15000	3	2018-08-23	2018-08-26	2018-08-29	17323	27050
2019-12-03	4	0	0	0	5	2019-12-03	2019-12-08	2019-12-08	9452	27050
2019-09-03	2	5	630000	15000	3	2019-09-03	2019-09-06	2019-09-09	25078	23403
2018-04-25	3	4	479000	35000	3	2018-04-25	2018-04-28	2018-05-05	15206	23403
2018-04-02	1	5	365000	15000	3	2018-04-02	2018-04-05	2018-04-08	17323	28514
2018-05-24	3	7	431000	25000	5	2018-05-24	2018-05-29	2018-06-03	15206	28514
2019-11-29	5	3	353000	5000	7	2019-11-29	2019-12-06	2019-12-07	5886	29664
2019-07-05	3	0	5000	5000	3	2019-07-05	2019-07-08	2019-07-09	9497	29664
2019-02-09	5	3	409000	25000	3	2019-02-09	2019-02-12	2019-02-17	20637	31685
2018-07-22	5	2	234000	20000	5	2018-07-22	2018-07-27	2018-07-31	6483	31685
2018-09-04	1	2	66000	0	5	2018-09-04	2018-09-09	2018-09-08	1497	24007
2018-07-11	3	0	0	0	3	2018-07-11	2018-07-14	2018-07-13	961	24174
2019-11-01	3	6	402000	0	7	2019-11-01	2019-11-08	2019-11-07	21854	31835
2018-12-12	3	3	408000	0	7	2018-12-12	2018-12-19	2018-12-13	18779	29032
2019-12-07	2	6	348000	0	3	2019-12-07	2019-12-10	2019-12-09	23627	31924
2018-04-01	3	7	896000	0	3	2018-04-01	2018-04-04	2018-04-04	6483	31230
2019-11-13	2	6	821000	5000	7	2019-11-13	2019-11-20	2019-11-21	12723	28239
2018-12-17	5	5	290000	0	7	2018-12-17	2018-12-24	2018-12-20	16353	25054
2018-01-09	3	0	0	0	3	2018-01-09	2018-01-12	2018-01-12	21908	25054
\.


--
-- Name: anggota anggota_pkey; Type: CONSTRAINT; Schema: komika; Owner: -
--

ALTER TABLE ONLY komika.anggota
    ADD CONSTRAINT anggota_pkey PRIMARY KEY (no_anggota);


--
-- Name: bank_mitra bank_mitra_pkey; Type: CONSTRAINT; Schema: komika; Owner: -
--

ALTER TABLE ONLY komika.bank_mitra
    ADD CONSTRAINT bank_mitra_pkey PRIMARY KEY (kode);


--
-- Name: komik komik_pkey; Type: CONSTRAINT; Schema: komika; Owner: -
--

ALTER TABLE ONLY komika.komik
    ADD CONSTRAINT komik_pkey PRIMARY KEY (id);


--
-- Name: menu menu_pkey; Type: CONSTRAINT; Schema: komika; Owner: -
--

ALTER TABLE ONLY komika.menu
    ADD CONSTRAINT menu_pkey PRIMARY KEY (id);


--
-- Name: pesanan_komik_baca pesanan_komik_baca_pkey; Type: CONSTRAINT; Schema: komika; Owner: -
--

ALTER TABLE ONLY komika.pesanan_komik_baca
    ADD CONSTRAINT pesanan_komik_baca_pkey PRIMARY KEY (nomor_urut, timestamp_baca, no_anggota);


--
-- Name: pesanan_komik_pinjam pesanan_komik_pinjam_pkey; Type: CONSTRAINT; Schema: komika; Owner: -
--

ALTER TABLE ONLY komika.pesanan_komik_pinjam
    ADD CONSTRAINT pesanan_komik_pinjam_pkey PRIMARY KEY (nomor_urut, timestamp_pinjam);


--
-- Name: pesanan_menu_trbaca pesanan_menu_trbaca_pkey; Type: CONSTRAINT; Schema: komika; Owner: -
--

ALTER TABLE ONLY komika.pesanan_menu_trbaca
    ADD CONSTRAINT pesanan_menu_trbaca_pkey PRIMARY KEY (nomor_urut, timestamp_baca, no_anggota);


--
-- Name: pesanan_menu_trpinjam pesanan_menu_trpinjam_pkey; Type: CONSTRAINT; Schema: komika; Owner: -
--

ALTER TABLE ONLY komika.pesanan_menu_trpinjam
    ADD CONSTRAINT pesanan_menu_trpinjam_pkey PRIMARY KEY (nomor_urut, timestamp_pinjam);


--
-- Name: petugas petugas_pkey; Type: CONSTRAINT; Schema: komika; Owner: -
--

ALTER TABLE ONLY komika.petugas
    ADD CONSTRAINT petugas_pkey PRIMARY KEY (username);


--
-- Name: produk produk_pkey; Type: CONSTRAINT; Schema: komika; Owner: -
--

ALTER TABLE ONLY komika.produk
    ADD CONSTRAINT produk_pkey PRIMARY KEY (id);


--
-- Name: transaksi_baca transaksi_baca_pkey; Type: CONSTRAINT; Schema: komika; Owner: -
--

ALTER TABLE ONLY komika.transaksi_baca
    ADD CONSTRAINT transaksi_baca_pkey PRIMARY KEY ("timestamp");


--
-- Name: transaksi_pinjam transaksi_pinjam_pkey; Type: CONSTRAINT; Schema: komika; Owner: -
--

ALTER TABLE ONLY komika.transaksi_pinjam
    ADD CONSTRAINT transaksi_pinjam_pkey PRIMARY KEY ("timestamp");


--
-- Name: transaksi_baca trigger_cek_buku_baca; Type: TRIGGER; Schema: komika; Owner: -
--

CREATE TRIGGER trigger_cek_buku_baca BEFORE INSERT ON komika.transaksi_baca FOR EACH ROW EXECUTE PROCEDURE komika.cek_buku_baca();


--
-- Name: transaksi_pinjam trigger_cek_buku_pinjam; Type: TRIGGER; Schema: komika; Owner: -
--

CREATE TRIGGER trigger_cek_buku_pinjam BEFORE INSERT ON komika.transaksi_pinjam FOR EACH ROW EXECUTE PROCEDURE komika.cek_buku_pinjam();


--
-- Name: transaksi_pinjam trigger_cek_durasi; Type: TRIGGER; Schema: komika; Owner: -
--

CREATE TRIGGER trigger_cek_durasi BEFORE INSERT ON komika.transaksi_pinjam FOR EACH ROW EXECUTE PROCEDURE komika.cek_durasi();


--
-- Name: komik komik_id_fkey; Type: FK CONSTRAINT; Schema: komika; Owner: -
--

ALTER TABLE ONLY komika.komik
    ADD CONSTRAINT komik_id_fkey FOREIGN KEY (id) REFERENCES komika.produk(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: menu menu_id_fkey; Type: FK CONSTRAINT; Schema: komika; Owner: -
--

ALTER TABLE ONLY komika.menu
    ADD CONSTRAINT menu_id_fkey FOREIGN KEY (id) REFERENCES komika.produk(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: pesanan_komik_baca pesanan_komik_baca_id_fkey; Type: FK CONSTRAINT; Schema: komika; Owner: -
--

ALTER TABLE ONLY komika.pesanan_komik_baca
    ADD CONSTRAINT pesanan_komik_baca_id_fkey FOREIGN KEY (id) REFERENCES komika.produk(id) ON UPDATE CASCADE;


--
-- Name: pesanan_komik_baca pesanan_komik_baca_no_anggota_fkey; Type: FK CONSTRAINT; Schema: komika; Owner: -
--

ALTER TABLE ONLY komika.pesanan_komik_baca
    ADD CONSTRAINT pesanan_komik_baca_no_anggota_fkey FOREIGN KEY (no_anggota) REFERENCES komika.anggota(no_anggota);


--
-- Name: pesanan_komik_baca pesanan_komik_baca_timestamp_baca_fkey; Type: FK CONSTRAINT; Schema: komika; Owner: -
--

ALTER TABLE ONLY komika.pesanan_komik_baca
    ADD CONSTRAINT pesanan_komik_baca_timestamp_baca_fkey FOREIGN KEY (timestamp_baca) REFERENCES komika.transaksi_baca("timestamp") ON UPDATE CASCADE;


--
-- Name: pesanan_komik_pinjam pesanan_komik_pinjam_id_fkey; Type: FK CONSTRAINT; Schema: komika; Owner: -
--

ALTER TABLE ONLY komika.pesanan_komik_pinjam
    ADD CONSTRAINT pesanan_komik_pinjam_id_fkey FOREIGN KEY (id) REFERENCES komika.produk(id) ON UPDATE CASCADE;


--
-- Name: pesanan_komik_pinjam pesanan_komik_pinjam_no_anggota_fkey; Type: FK CONSTRAINT; Schema: komika; Owner: -
--

ALTER TABLE ONLY komika.pesanan_komik_pinjam
    ADD CONSTRAINT pesanan_komik_pinjam_no_anggota_fkey FOREIGN KEY (no_anggota) REFERENCES komika.anggota(no_anggota) ON UPDATE CASCADE;


--
-- Name: pesanan_komik_pinjam pesanan_komik_pinjam_timestamp_pinjam_fkey; Type: FK CONSTRAINT; Schema: komika; Owner: -
--

ALTER TABLE ONLY komika.pesanan_komik_pinjam
    ADD CONSTRAINT pesanan_komik_pinjam_timestamp_pinjam_fkey FOREIGN KEY (timestamp_pinjam) REFERENCES komika.transaksi_pinjam("timestamp") ON UPDATE CASCADE;


--
-- Name: pesanan_menu_trbaca pesanan_menu_trbaca_id_fkey; Type: FK CONSTRAINT; Schema: komika; Owner: -
--

ALTER TABLE ONLY komika.pesanan_menu_trbaca
    ADD CONSTRAINT pesanan_menu_trbaca_id_fkey FOREIGN KEY (id) REFERENCES komika.produk(id) ON UPDATE CASCADE;


--
-- Name: pesanan_menu_trbaca pesanan_menu_trbaca_no_anggota_fkey; Type: FK CONSTRAINT; Schema: komika; Owner: -
--

ALTER TABLE ONLY komika.pesanan_menu_trbaca
    ADD CONSTRAINT pesanan_menu_trbaca_no_anggota_fkey FOREIGN KEY (no_anggota) REFERENCES komika.anggota(no_anggota) ON UPDATE CASCADE;


--
-- Name: pesanan_menu_trbaca pesanan_menu_trbaca_timestamp_baca_fkey; Type: FK CONSTRAINT; Schema: komika; Owner: -
--

ALTER TABLE ONLY komika.pesanan_menu_trbaca
    ADD CONSTRAINT pesanan_menu_trbaca_timestamp_baca_fkey FOREIGN KEY (timestamp_baca) REFERENCES komika.transaksi_baca("timestamp") ON UPDATE CASCADE;


--
-- Name: pesanan_menu_trpinjam pesanan_menu_trpinjam_id_fkey; Type: FK CONSTRAINT; Schema: komika; Owner: -
--

ALTER TABLE ONLY komika.pesanan_menu_trpinjam
    ADD CONSTRAINT pesanan_menu_trpinjam_id_fkey FOREIGN KEY (id) REFERENCES komika.produk(id) ON UPDATE CASCADE;


--
-- Name: pesanan_menu_trpinjam pesanan_menu_trpinjam_no_anggota_fkey; Type: FK CONSTRAINT; Schema: komika; Owner: -
--

ALTER TABLE ONLY komika.pesanan_menu_trpinjam
    ADD CONSTRAINT pesanan_menu_trpinjam_no_anggota_fkey FOREIGN KEY (no_anggota) REFERENCES komika.anggota(no_anggota) ON UPDATE CASCADE;


--
-- Name: pesanan_menu_trpinjam pesanan_menu_trpinjam_timestamp_pinjam_fkey; Type: FK CONSTRAINT; Schema: komika; Owner: -
--

ALTER TABLE ONLY komika.pesanan_menu_trpinjam
    ADD CONSTRAINT pesanan_menu_trpinjam_timestamp_pinjam_fkey FOREIGN KEY (timestamp_pinjam) REFERENCES komika.transaksi_pinjam("timestamp") ON UPDATE CASCADE;


--
-- Name: transaksi_baca transaksi_baca_kode_fkey; Type: FK CONSTRAINT; Schema: komika; Owner: -
--

ALTER TABLE ONLY komika.transaksi_baca
    ADD CONSTRAINT transaksi_baca_kode_fkey FOREIGN KEY (kode) REFERENCES komika.bank_mitra(kode) ON UPDATE CASCADE;


--
-- Name: transaksi_baca transaksi_baca_no_anggota_fkey; Type: FK CONSTRAINT; Schema: komika; Owner: -
--

ALTER TABLE ONLY komika.transaksi_baca
    ADD CONSTRAINT transaksi_baca_no_anggota_fkey FOREIGN KEY (no_anggota) REFERENCES komika.anggota(no_anggota) ON UPDATE CASCADE;


--
-- Name: transaksi_pinjam transaksi_pinjam_kode_fkey; Type: FK CONSTRAINT; Schema: komika; Owner: -
--

ALTER TABLE ONLY komika.transaksi_pinjam
    ADD CONSTRAINT transaksi_pinjam_kode_fkey FOREIGN KEY (kode) REFERENCES komika.bank_mitra(kode) ON UPDATE CASCADE;


--
-- Name: transaksi_pinjam transaksi_pinjam_no_anggota_fkey; Type: FK CONSTRAINT; Schema: komika; Owner: -
--

ALTER TABLE ONLY komika.transaksi_pinjam
    ADD CONSTRAINT transaksi_pinjam_no_anggota_fkey FOREIGN KEY (no_anggota) REFERENCES komika.anggota(no_anggota) ON UPDATE CASCADE;


--
-- PostgreSQL database dump complete
--

