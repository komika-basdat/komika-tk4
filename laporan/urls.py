from django.urls import path
from .views import laporan

app_name = 'laporan'

urlpatterns = [
    path('', laporan, name='home'),
]